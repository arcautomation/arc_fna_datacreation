package com.arc.fna.testscripts;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FnaContactTabPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.FnaRecyclebinTabPage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class SearchLoadTestScript {
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	FnaRecyclebinTabPage fnarecyclebintabpage;
	FnaContactTabPage fnacontacttabpage;
	
	
	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {
		
		if(browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome")) { 
		File dest = new File("./drivers/win/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "false"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	

	/** TC_001 (Solar_Search): Create n no of collections
	
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = false, description = "TC_001 (Solar_Search): Create n no of collections")
	public void verifyCreateNOfCollection() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_001 (Solar_Search): Create n no of collections");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("EmailforSearch");
			String pWord = PropertyReader.getProperty("Passwordforsearch");
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();		
			
			String numberofcollectn=PropertyReader.getProperty("NoOfCollection");
			int n=Integer.parseInt(numberofcollectn);
			for(int i=1;i<=n;i++) {
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			}
			
			}
			catch(Exception e)
			{
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				driver.quit();
			}
		}
	/** TC_002 (Solar_Search): Add 300 folder_1 to collection

	 * @throws Exception
	 */
	@Test(priority = 1, enabled = false, description = "TC_002 (Solar_Search): Add 300 folder_1 to collection")
	public void verifyAdd300Folder_1ToCollection() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_002 (Solar_Search): Add 300 folder_1 to collection");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("EmailforSearch");
			String pWord = PropertyReader.getProperty("Passwordforsearch");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("ColleNameforsearch");
			fnaHomePage.selectcollection(Collection_Name);	
			String numberoffolder1=PropertyReader.getProperty("NoofFolder_1");
			int n=Integer.parseInt(numberoffolder1);
			for(int i =1;i<=n;i++) {
			  String FolderName= fnaHomePage.Random_Foldername();		
		      Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
		      fnaHomePage.selectMainCollection();
			}
		}
			catch(Exception e)
			{
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				driver.quit();
			}
		}
	/** TC_003 (Solar_Search): Add 300 folder_2 to collection

	 * @throws Exception
	 */
	@Test(priority = 2, enabled = false, description = "TC_003 (Solar_Search): Add 300 folder_2 to collection")
	public void verifyAdd300Folder_2ToCollection() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_003 (Solar_Search): Add 300 folder_2 to collection");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("EmailforSearch");
			String pWord = PropertyReader.getProperty("Passwordforsearch");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("ColleNameforsearch_folder2");
			fnaHomePage.selectcollection(Collection_Name);	
			String numberoffolder1=PropertyReader.getProperty("NoofFolder_2");
			int n=Integer.parseInt(numberoffolder1);
			for(int i =1;i<=n;i++) {
			  String FolderName= fnaHomePage.Random_Foldername();		
		      Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
		      fnaHomePage.selectMainCollection();
			}
		}
			catch(Exception e)
			{
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				driver.quit();
			}
		}
	/** TC_004 (Solar_Search): Add 300 folder_3 to collection

	 * @throws Exception
	 */
	@Test(priority = 3, enabled = false, description = "TC_004 (Solar_Search): Add 300 folder_3 to collection")
	public void verifyAdd300Folder_3ToCollection() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_004 (Solar_Search): Add 300 folder_3 to collection");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("EmailforSearch");
			String pWord = PropertyReader.getProperty("Passwordforsearch");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("ColleNameforsearch_folder3");
			fnaHomePage.selectcollection(Collection_Name);	
			String numberoffolder1=PropertyReader.getProperty("NoofFolder_3");
			int n=Integer.parseInt(numberoffolder1);
			for(int i =1;i<=n;i++) {
			  String FolderName= fnaHomePage.Random_Foldername();		
		      Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
		      fnaHomePage.selectMainCollection();
			}
		}
			catch(Exception e)
			{
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				driver.quit();
			}
		}
	
	/** TC_005 (Solar_Search): Add 100 folder_1 to collection

	 * @throws Exception
	 */
	@Test(priority = 4, enabled = false, description = "TC_004 (Solar_Search): Add 100 folder_1 to collection")
	public void verifyAdd100Folder_1ToCollection() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_005 (Solar_Search): Add 100 folder_1 to collection");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("EmailforSearch");
			String pWord = PropertyReader.getProperty("Passwordforsearch");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("ColleNameforsearch_folder100");
			fnaHomePage.selectcollection(Collection_Name);	
			String numberoffolder1=PropertyReader.getProperty("NofFolder_1_100");
			int n=Integer.parseInt(numberoffolder1);
			for(int i =1;i<=n;i++) {
			  String FolderName= fnaHomePage.Random_Foldername();		
		      Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
		      fnaHomePage.selectMainCollection();
			}
		}
			catch(Exception e)
			{
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				driver.quit();
			}
		}
	

	/** TC_006 (Solar_Search): Add 100 folder_2 to collection

	 * @throws Exception
	 */
	@Test(priority = 5, enabled = false, description = "TC_006 (Solar_Search): Add 100 folder_2 to collection")
	public void verifyAdd100Folder_2ToCollection() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_006 (Solar_Search): Add 100 folder_2 to collection");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("EmailforSearch");
			String pWord = PropertyReader.getProperty("Passwordforsearch");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("ColleNameforsearch_folder_2_100");
			fnaHomePage.selectcollection(Collection_Name);	
			String numberoffolder1=PropertyReader.getProperty("NofFolder_2_100");
			int n=Integer.parseInt(numberoffolder1);
			for(int i =1;i<=n;i++) {
			  String FolderName= fnaHomePage.Random_Foldername();		
		      Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
		      fnaHomePage.selectMainCollection();
			}
		}
			catch(Exception e)
			{
				e.getCause();
				Log.exception(e, driver);
			}
			finally
			{
				Log.endTestCase();
				driver.quit();
			}
		}
	

	/** TC_07 (Solar_Search): Verify Upload of multiple file
	 * chnage collection folder and file folder according to requirement
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 6, enabled = false, description = "TC_07 (Solar_Search): Verify Upload 200 file to any one folder of 100 folder.")
	public void verifyUploadofFile_200_Folder1() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_07 (Solar_Search): Verify Upload 200 file to any one folder of 100 folder");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("EmailforSearch");
			String pWord = PropertyReader.getProperty("Passwordforsearch");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("ColleNameforsearch");
			fnaHomePage.selectcollection(Collection_Name);	
			String Foldername=PropertyReader.getProperty("Folder_For_200");
	        //method to select folder 
			fnaHomePage.Select_Folder(Foldername);		
			
			
	         
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
		    String FolderPathoffile = Path.getAbsolutePath().toString();
			
		    fnaHomePage.UploadFile(FolderPathoffile);
			
		}
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			//driver.quit();
		}
	}
	
	
	  /** TC_003(Contact):Verify successful creation of new contact.
     * @throws Exception 
      * 
      */
     @Test(priority=2,enabled=true,description="Verify successful creation of new contact.")
     public void verifySuccessfullCreationNewContact() throws Exception {
    	 try {
    		     Log.testCaseInfo("TC_003(Contact):Verify successful creation of new contact.");
    		     loginPage = new LoginPage(driver).get();		
    				String uName = PropertyReader.getProperty("EmailforSearch");
    				String pWord = PropertyReader.getProperty("Passwordforsearch");			
    				fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
    				fnaHomePage.loginValidation();	
    				fnaHomePage.contactIconPresent();
    	     	 fnacontacttabpage=fnaHomePage.contactClick();
    	     	 Log.assertThat(fnacontacttabpage.AddressPage_IsLanded(),"AddressPage is landed after clicking on ContactIcon button","AddressPage is not landed after clicking on ContactIcon button");
    	     	
    	     	String numberoffolder1=PropertyReader.getProperty("noofcontact");
    			int n=Integer.parseInt(numberoffolder1);
    			for(int i =1;i<=n;i++) {
    				String contactname=fnacontacttabpage.Random_Contactname();
       	      	 String Lastname1=fnacontacttabpage.Random_LastName();
       	     	 String emailid=fnacontacttabpage.Random_Email();
    	     	 Log.assertThat(fnacontacttabpage.CreateNewContact(contactname,emailid,Lastname1),"New Contact has been created","New contact has not been added and also not available under contactnamelist");
    			}
    		 }
    	 catch(Exception e){
    		 e.getCause();
             Log.exception(e, driver);
    		 
    	 }
    	 finally {
    		 
    		 Log.endTestCase();
             driver.quit();
    	 }
     }	 
} 
