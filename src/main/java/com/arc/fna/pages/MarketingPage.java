package com.arc.fna.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;
import com.gargoylesoftware.htmlunit.javascript.host.Iterator;
import java.util.List;
import java.util.Set;



public class MarketingPage extends LoadableComponent<MarketingPage> {
WebDriver driver;
private  boolean isPageLoaded;

/** Find Elements using FindBy annotation
 * 
 */
@FindBy(css="li.head-signup:nth-child(3) > a:nth-child(1)")
@CacheLookup
WebElement Freetrialbtn;

	
@FindBy(css="li.head-signin:nth-child(2) > a:nth-child(1)")
@CacheLookup
WebElement Loginbtn;





@Override
protected void load() {
	// TODO Auto-generated method stub
	isPageLoaded=true;
	SkySiteUtils.waitForElement(driver,Freetrialbtn,10);
	Log.message("Page is loaded");
}


@Override
protected void isLoaded() throws Error {
	// TODO Auto-generated method stub
	if (!isPageLoaded)
	{
		Assert.fail();
	}
}
/**
 * Declaring constructor for initializing web elements using PageFactory class.
 * @param driver
 */
public MarketingPage(WebDriver driver){
	this.driver=driver;
	PageFactory.initElements(this.driver, this);
}


/** 
 * Method written for landing into login screen from marketing page of the skysite
 * Without modifying the before method this method can be used
 * @return
 * Created By trinanjwan
 */


public LoginPage landingofLoginfromMarketing()


{
	
	String marketingurl=PropertyReader.getProperty("MarketingpageURL");
	driver.get(marketingurl);
	String currentWindowID = driver.getWindowHandle();
	Log.message("Current window ID is: "+currentWindowID);
	SkySiteUtils.waitForElement(driver, Loginbtn, 10);
	Log.message("The Login button is appeared now");
	Loginbtn.click();
	 Set <String> windowIDs = driver.getWindowHandles();
     for(String st : windowIDs){
         if(! st.equalsIgnoreCase( currentWindowID )){
             driver.switchTo().window( st );
         }
     }
         return new LoginPage(driver).get();
	
}
}

