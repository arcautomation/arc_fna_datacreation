package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class CollectionTaskPage extends LoadableComponent<CollectionTaskPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public CollectionTaskPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	
	/**
	 * Method written for Random(Task)
	 *  Scripted By: Sekhar	
	 * @return
	 */

	public String Random_Task()
	{

		String str = "Task";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	/**
	 * Method written for Random(Subject)
	 *  Scripted By: Sekhar
	 * @return
	 */

	public String Random_Subject()
	{

		String str = "Subject";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	@FindBy(css = "#ProjectMenu1_Task>a")
	WebElement btntask;	
	
	@FindBy(xpath = "//*[@id='SelTaskAssignList']/div[1]/table/tbody/tr[2]/td[2]/div")
	WebElement SelTaskAssignList;
	
	@FindBy(css = "#btnNewTask")
	WebElement btnNewTask;
	
	@FindBy(css = "#txtTaskName")
	WebElement txtTaskName;
	
	@FindBy(css = "#txtTaskSubject")
	WebElement txtTaskSubject;
	
	@FindBy(css = "#txtDate")
	WebElement txtDate;
	
	@FindBy(css = ".day.active")
	WebElement dayactive;
	
	@FindBy(css = "#Img1")
	WebElement datefar;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/table/thead/tr[1]/th[3]")
	WebElement datenext;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/table/tbody/tr[2]/td[4]")
	WebElement duedate;
	
	@FindBy(xpath = "(//button[@class='btn dropdown-toggle btn-default'])[1]")
	WebElement  btndefault;
		
	@FindBy(xpath = "(//span[@class='text'])[2]")
	WebElement  btninprogress;
	
	@FindBy(xpath = "//body[@id='tinymce']")
	WebElement tinymce;
	
	@FindBy(css = "#btnAddTUFromAB")
	WebElement btnAddTUFromAB;
	
	@FindBy(css = "#rbTeamMembers")
	WebElement rbTeamMembers;
	
	@FindBy(css = "#btn_addMeemberAndclose")
	WebElement btnaddMeemberAndclose;
	
	/** 
     * Method written for Adding Task with subject
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	public boolean Add_Task_withSubject(String TaskName,String SubjectName,String Contact,String Email)
	{
		boolean result1=false;  		
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btntask.click();
		Log.message("Task button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));//switch to my frame
 		Log.message("Swich to I Frame Success!!"); 
 		SkySiteUtils.waitTill(3000);
 		btnNewTask.click();//click on the add new task
 		Log.message("Add button has been clicked");
		SkySiteUtils.waitTill(5000);
 		SkySiteUtils.waitForElement(driver, SelTaskAssignList, 120);  
 		txtTaskName.sendKeys(TaskName);
 		Log.message("Task name has been entered");
 		txtTaskSubject.sendKeys(SubjectName);
 		Log.message("Subject name has been entered");
 		SkySiteUtils.waitTill(2000);
 		//driver.findElement(By.xpath("(//i[@class='icon icon-calendar'])[1]")).click();
 		//SkySiteUtils.waitTill(3000);
 		txtDate.click();
 		Log.message("date button has been clicked");
 		SkySiteUtils.waitTill(2000);	
 		dayactive.click();
 		Log.message("active day has been clicked");
 		SkySiteUtils.waitTill(2000);	 		
 		datefar.click();	
 		Log.message("date icon button has been clicked");
 		SkySiteUtils.waitTill(3000); 		
 		datenext.click();
 		Log.message("forward date button has been clicked");
 		SkySiteUtils.waitTill(3000);
 		duedate.click();
 		Log.message("due date has been clicked");
 		SkySiteUtils.waitTill(3000);
 		btndefault.click();
 		Log.message("default drop down button has been clicked");
 		btninprogress.click();
 		Log.message("Inprogress has been clicked");
 		SkySiteUtils.waitTill(3000);
 		driver.switchTo().frame(driver.findElement(By.id("txtNote_ifr")));//switch to text note frame
 		Log.message("Swich to II Frame Success!!"); 
 		SkySiteUtils.waitTill(3000);
 		tinymce.sendKeys(SubjectName);//enter the Description
 		Log.message("Description has been entered");
 		SkySiteUtils.waitTill(5000); 		
 		driver.switchTo().defaultContent();
 		driver.switchTo().frame(driver.findElement(By.id("myFrame")));//switch to my frame
 		Log.message("Swich to I Frame Success!!"); 	
 		SkySiteUtils.waitTill(3000);
 		btnAddTUFromAB.click();//click on Add Member
 		Log.message("Add member has been clicked");
  		SkySiteUtils.waitTill(3000);
  		driver.switchTo().defaultContent(); 		
  		rbTeamMembers.click();//click on radio button
  		Log.message("collection team member radio button has been clicked");
  		SkySiteUtils.waitTill(8000);  		
  		int Checkbox_Count=0;
  		int i = 0;
  		List<WebElement> allElements = driver.findElements(By.xpath("//*[@id='divGridPTeam']/div[2]/table/tbody/tr/td[1]/img"));//Taking check box Count
  		for (WebElement Element : allElements)
  		{ 
  			Checkbox_Count = Checkbox_Count+1; 	
  		}
  		Log.message("Check box Count is: "+Checkbox_Count);
  		//Getting Folder Names and machine
  		for(i = 1;i<=Checkbox_Count;i++)
  		{					
  			String ConatctName=driver.findElement(By.xpath("(//*[@id='divGridPTeam']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
  			Log.message("Contact Name is:"+ConatctName);
  			String Act_MailID=driver.findElement(By.xpath("(//*[@id='divGridPTeam']/div[2]/table/tbody/tr/td[4])["+i+"]")).getText().toString();
  			Log.message("Act Email ID is:"+Act_MailID);			
  			SkySiteUtils.waitTill(3000);
  			if((ConatctName.contentEquals(Contact))&&(Act_MailID.contentEquals(Email)))
  			{	
  				result1=true;
  				Log.message("Expected Contact Selected Successfully");	
  				driver.findElement(By.xpath("(//*[@id='divGridPTeam']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();//select Check box
  				SkySiteUtils.waitTill(5000);
  				break;
  			}	
  		}
  		if(result1==true)
  		{
  			Log.message("Expected Contact Selected Successfully");	
  		}
  		else
  		{
  			Log.message("Expected Contact Selected UnSuccessfully");	
  		}
  		SkySiteUtils.waitTill(5000);
  		btnaddMeemberAndclose.click();
  		Log.message("Add member & close has been clicked");
  		SkySiteUtils.waitTill(5000);
  		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
  		Log.message("switch to my frame");
  		SkySiteUtils.waitTill(3000);
  		String Exp_Contact = driver.findElement(By.xpath("//*[@id='SelTaskAssignList']/div[2]/table/tbody/tr[2]/td[2]/a")).getText();
  		Log.message("Exp Contact name is:"+Exp_Contact);	
  		SkySiteUtils.waitTill(5000);
  		if(Exp_Contact.contentEquals(Contact)) 	  			
  			return true;	  			
  		else
  			Log.message("Expected Contact Unsuccessfull");
  			return false;  		
	 }
	
	 @FindBy(xpath = "//button[@id='btnFileUpload']")
	 WebElement btnFileUpload;
	  
	 @FindBy(xpath = "(//i[@class='icon icon-add'])[5]")
	 WebElement btniconiconadd;
	
	 @FindBy(xpath = "//*[@id='FileActionMenu']/li[1]/a")
	 WebElement btnFileActionMenu;
	 
	 @FindBy(xpath = "//input[@name='qqfile']")
	 WebElement btnselectfile;
	 
	 @FindBy(xpath = "//*[@id='FileActionMenu']/li[2]/a")
	 WebElement btnFileActioncoll;	
	 
	 @FindBy(xpath = "//*[@id='divFilesGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
	 WebElement chkboxdivFilesGrid;
		 
	 @FindBy(xpath = "(//input[@class='btn btn-primary'])[2]")
	 WebElement btnbtnprimary;
	
	 @FindBy(xpath = "//input[@id='btnSave']")
	 WebElement btnbtnSave;
	 ////button[@id='bdnReset']
	 @FindBy(xpath = "//button[@id='bdnReset']")
	 WebElement btnbdnReset;
	 
	/** 
     * Method written for Attach file(internal and external)in task
     * Scripted By: Sekhar     
	 * @throws IOException 
     * @throws AWTException 
     */
	public boolean Attach_file_task(String FolderPath,String TaskName,String SubjectName,String Contact) throws IOException
	{
		boolean result1=false;  
		boolean result2=false;
  		SkySiteUtils.waitTill(3000);
  		btniconiconadd.click();
  		Log.message("Add button has been clicked");
  		SkySiteUtils.waitTill(3000);
  		btnFileActionMenu.click();
  		Log.message("Attach from your computer button has been clicked");
  		SkySiteUtils.waitTill(2000);  		 				
  		String parentHandle = driver.getWindowHandle(); //Getting parent window
  		Log.message(parentHandle);
  		for(String winHandle : driver.getWindowHandles())
  		{
  			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
  		}
  		SkySiteUtils.waitTill(10000);
  		driver.switchTo().defaultContent();
  		btnselectfile.click();//click on choose File button
  		SkySiteUtils.waitTill(5000);	
	
  		// Writing File names into a text file for using in AutoIT Script
  		BufferedWriter output;
  		randomFileName rn = new randomFileName();
  		//String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
  		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
  		output = new BufferedWriter(new FileWriter(tmpFileName, true));
  			 
  		String expFilename = null;
  		File[] files = new File(FolderPath).listFiles();
  		
  		for (File file : files) 
  		{
  			if (file.isFile()) 
  			{
  				expFilename = file.getName();// Getting File Names into a variable
  				Log.message("Expected File name is:" + expFilename);
  				output.append('"' + expFilename + '"');
  				output.append(" ");
  				SkySiteUtils.waitTill(5000);
  			}	
  		}
  		output.flush();
  		output.close();
  		
  		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
  		// Executing .exe autoIt file
  		Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
  		Log.message("AutoIT Script Executed!!");
  		SkySiteUtils.waitTill(30000);
  			 
  		try 
  		{
  			File file = new File(tmpFileName);
  			if (file.delete()) 
  			{
  				Log.message(file.getName() + " is deleted!");
  			}
  			else 
  			{
  				Log.message("Delete operation is failed.");
  			}
  		} 
  		catch (Exception e)
  		{
  			Log.message("Exception occured!!!" + e);
  		}
  		SkySiteUtils.waitTill(8000);
  		btnFileUpload.click();// Clicking on Upload button
  		Log.message("Upload button has been clicked");
  		SkySiteUtils.waitTill(30000);
  		driver.switchTo().window(parentHandle);// Switch back to folder page
  		SkySiteUtils.waitTill(5000);  		
  		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
  		SkySiteUtils.waitTill(5000);
  		String Exp_FileName=driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[2]/td[1]/a[2]")).getText();
  		Log.message("Exp File Name is:"+Exp_FileName);
  		if(driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[2]/td[1]/a[2]")).isDisplayed())
  		{
  			result1=true;
  			Log.message("Attachment File From Computer Successfully");
  		}	
  		else
  		{
  			result1=false;
  			Log.message("Attachment File From Computer UnSuccessfully");  	
  		}
  		SkySiteUtils.waitTill(2000);
  		btniconiconadd.click();
  		Log.message("Add button has been clicked");
  		SkySiteUtils.waitTill(2000);
  		btnFileActioncoll.click();
  		Log.message("Attach Fron your collection button has been clicked");
  		SkySiteUtils.waitTill(2000);
  		String parentHandle1 = driver.getWindowHandle(); //Getting parent window
  		Log.message(parentHandle1);
  		for(String winHandle : driver.getWindowHandles())
  		{
  			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
  		}
  		SkySiteUtils.waitTill(2000);
  		driver.switchTo().defaultContent();
  		chkboxdivFilesGrid.click();
  		Log.message("all check box button has been clicked");
  		SkySiteUtils.waitTill(2000);
  		btnbtnprimary.click(); 
  		Log.message("Add&close button has been clicked");
  		SkySiteUtils.waitTill(3000);				 
  		driver.switchTo().window(parentHandle1);//Switch back to folder page 	
  		SkySiteUtils.waitTill(5000);
  		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
  		SkySiteUtils.waitTill(5000);
  		String Exp_FileName2=driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[3]/td[1]/a[2]")).getText();
  		Log.message("Exp File name2 is:"+Exp_FileName2);
  		if(driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[3]/td[1]/a[2]")).isDisplayed())
  		{
  			result2=true;
  			Log.message("Attach From Project Successfully");									
  		}
  		else
  		{
  			result2=false;
  			Log.message("Attach From Project UnSuccessfully");		
  		}
  		SkySiteUtils.waitTill(3000);
  		btnbtnSave.click();
  		Log.message("create task button has been clicked");
  		SkySiteUtils.waitTill(5000);
  		btnbdnReset.click();
  		Log.message("Reset button has been clicked");
  		SkySiteUtils.waitTill(5000);
  		String Exp_TaskName=driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[3]/a[2]")).getText();
  		Log.message("Exp Task Name is:"+Exp_TaskName);
  		SkySiteUtils.waitTill(2000);
  		String Subject_Name=driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[4]")).getText();
  		Log.message("Subject Name is:"+Subject_Name);
  		SkySiteUtils.waitTill(2000);
  		String Assigned_To=driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[6]")).getText();
  		Log.message("Assigned To is:"+Assigned_To);
  		if((Exp_TaskName.contentEquals(TaskName))&&(Subject_Name.contentEquals(SubjectName))
  				&&(Assigned_To.contentEquals(Contact)))
  			return true;
  		else
  			return false;			
	 }	
}